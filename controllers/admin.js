/**
 * GET /
 * Admin page.
 */
exports.index = (req, res) => {
    res.send({title: 'Home'});
};