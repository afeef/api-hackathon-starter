const express = require('express');

const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const dotenv = require('dotenv');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const expressValidator = require('express-validator');
const expressStatusMonitor = require('express-status-monitor');
const sass = require('node-sass-middleware');
const cors = require('cors');
let Acl = require('acl');

dotenv.load({ path: '.env' });

const homeController = require('./controllers/home');
const userController = require('./controllers/user');
const adminController = require('./controllers/admin');
const passportConfig = require('./config/passport');

const app = express();

// Generic debug logger for node_acl
const acl_logger = () => {
  return {
      debug: function( msg ) {
          console.log( '-DEBUG-', msg );
      }
  };
};

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI);

mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

app.use(cors());
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(expressStatusMonitor());
app.use(compression());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
}));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: process.env.MONGODB_URI,
    autoReconnect: true,
    clear_interval: 3600,
  }),
}));
app.use(passport.initialize());
app.use(flash());
// app.use((req, res, next) => {
//   if (req.path === '/api/upload') {
//     next();
//   } else {
//     lusca.csrf()(req, res, next);
//   }
// });
// app.use(lusca.xframe('SAMEORIGIN'));
// app.use(lusca.xssProtection(false));
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => {
  if (!req.user &&
      req.path !== '/login' &&
      req.path !== '/signup' &&
      !req.path.match(/^\/auth/) &&
      !req.path.match(/\./)) {
    req.session.returnTo = req.path;
  } else if (req.user &&
      req.path === '/account') {
    req.session.returnTo = req.path;
  }

  next();
});
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));


const set_roles = (acl) => {
  acl.allow([
    {
      roles: 'admin',
      allows: [
        { resources: ['/admin', '/account', '/auth'], permissions: '*' }
      ]
    }, {
      roles: 'user',
      allows: [
        { resources: ['/account', '/auth'], permissions: '*' }
      ]
    }, {
      roles: 'guest',
      allows: []
    }
  ]);
  
  acl.addUserRoles(0, 'guest');  
  acl.addRoleParents( 'user', 'guest' );
  acl.addRoleParents( 'admin', 'user' );
};

const set_routes = (acl) => {
  app.get('/userRoles', (req, res) => {
    acl.userRoles(passportConfig.get_user_id( req, resp ), function( error, roles ){
      response.send({user: req.user, roles: roles});
    });
  });
  
  app.get('/logout', (req, res) => {
    request.logout();
    response.send( 'Logged out!' );
  });
  
  app.get('/', homeController.index);
  app.get('/permissions', (req, res) => {
    acl.allowedPermissions(passportConfig.get_user_id(req, res), ['/admin', '/account', '/auth'], (err, permissions) => {
      res.send({permissions, user: passportConfig.get_user_id(req, res)});
    });
  });
  
  app.get('/roles', (req, res) => {
    acl.userRoles(passportConfig.get_user_id(req, res), (err, roles) => {
      res.send({ roles });
    })
  });
  
  app.get('/allow/:user_id/:role', passportConfig.authenticate, (req, res, next ) => {
    acl.addUserRoles(req.params.user_id, req.params.role);
    res.send(req.params.user_id + ' is a ' + req.params.role);
  });
  
  app.get('/disallow/:user_id/:role', passportConfig.authenticate, (req, res, next) => {
    acl.removeUserRoles(req.params.user_id, req.params.role );
    res.send(req.params.user_id + ' is not a ' + req.params.role + ' anymore.' );
  });
  
  app.get('/admin/users', [passportConfig.authenticate, acl.middleware(1, passportConfig.get_user_id)], (req, res) => {
    acl.roleUsers('admin', (err, roleUsers) => {
      res.send({ roleUsers });
    })
  });

  app.get('admin/users/:user_id/permissions', [passportConfig.authenticate, acl.middleware(1, passportConfig.get_user_id)], (req, res) => {
    acl.allowedPermissions(req.params.user_id, ['/admin', '/account', '/auth'], (err, permissions) => {
      res.send({permissions, user: passportConfig.get_user_id(req, res)});
    });
  });
  
  app.get('/admin', passportConfig.authenticate, acl.middleware(1, passportConfig.get_user_id), adminController.index);
  app.post('/users/register', userController.postSignup);
  app.post('/users/authenticate', userController.login, passportConfig.generateToken, passportConfig.respond);
  app.get('/logout', userController.logout);
  app.post('/forgot', userController.postForgot);
  app.post('/reset/:token', userController.postReset);
  app.get('/account', passportConfig.authenticate, userController.getAccount);
  app.post('/account/profile', passportConfig.authenticate, userController.postUpdateProfile);
  app.post('/account/password', passportConfig.authenticate, userController.postUpdatePassword);
  app.delete('/account/delete', passportConfig.authenticate, userController.postDeleteAccount);
  app.get('/account/unlink/:provider', passportConfig.authenticate, userController.getOauthUnlink);
  
  app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email', 'public_profile'] }));
  app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), (req, res) => {
    res.redirect(req.session.returnTo || '/');
  });
  app.get('/auth/twitter', passport.authenticate('twitter'));
  app.get('/auth/twitter/callback', passport.authenticate('twitter', { failureRedirect: '/login' }), (req, res) => {
    res.redirect(req.session.returnTo || '/');
  });
  
  /**
   * Error Handler.
   */
  app.use(errorHandler());
  
  /**
   * Start Express server.
   */
  app.listen(app.get('port'), () => {
    console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env')); 
    console.log('  Press CTRL-C to stop\n');
  });
};

mongoose.connection.once('open', () => {
  let acl = new Acl(new Acl.mongodbBackend(mongoose.connection.db, 'acl_'), acl_logger());
  set_roles(acl);
  set_routes(acl);
});

module.exports = app;
